var Firebase = require("./firebase-node.js");
//var Firebase = require("firebase");

/**
 * This simple sample has no external dependencies or session management, and shows the most basic
 * example of how to create a Lambda function for handling Alexa Skill requests.
 *
 * Examples:
 * One-shot model:
 *  User: "Alexa, ask Inspiration for a quote"
 *  Alexa: "Here's your space fact: ..."
 */


 * App ID for the skill
 */
var APP_ID = "amzn1.echo-sdk-ams.app.0779f24a-27c9-4fea-b6a6-3d3d587435d5"; //replace with "amzn1.echo-sdk-ams.app.[your-unique-value-here]";


var ref = new Firebase("http://contact413.firebaseio.com/quotes");
var quoteArr = [];


/* The AlexaSkill prototype and helper functions */
var AlexaSkill = require('./AlexaSkill');

/* app is a child of AlexaSkill. */
var SpaceGeek = function () {
    AlexaSkill.call(this, APP_ID);
};

// Extend AlexaSkill
SpaceGeek.prototype = Object.create(AlexaSkill.prototype);
SpaceGeek.prototype.constructor = SpaceGeek;

SpaceGeek.prototype.eventHandlers.onSessionStarted = function (sessionStartedRequest, session) {
    console.log("SpaceGeek onSessionStarted requestId: " + sessionStartedRequest.requestId + ", sessionId: " + session.sessionId);
    // any initialization logic goes here
};

SpaceGeek.prototype.eventHandlers.onLaunch = function (launchRequest, session, response) {
    console.log("SpaceGeek onLaunch requestId: " + launchRequest.requestId + ", sessionId: " + session.sessionId);
    handleNewFactRequest(response);
};

/* Overridden to show that a subclass can override this function to teardown session state.*/
SpaceGeek.prototype.eventHandlers.onSessionEnded = function (sessionEndedRequest, session) {
    console.log("SpaceGeek onSessionEnded requestId: " + sessionEndedRequest.requestId
        + ", sessionId: " + session.sessionId);
    // any cleanup logic goes here
};

SpaceGeek.prototype.intentHandlers = {
    "GetNewFactIntent": function (intent, session, response) {
//          
//        var itemVal     = 0;   
//    var itemSlot    = intent.slots.Number;
//    if (itemSlot && itemSlot.value){
//        itemVal = itemSlot.value;
//    }
        console.log(intent)
        handleNewFactRequest(response);
    },

    "AMAZON.HelpIntent": function (intent, session, response) {
        response.ask("You can ask GoodQuotes tell me a quote, or, you can say exit... What can I help you with?", "What can I help you with?");
    },
    "AMAZON.StopIntent": function (intent, session, response) {
        var speechOutput = "Goodbye";
        response.tell(speechOutput);
    },
    "AMAZON.CancelIntent": function (intent, session, response) {
        var speechOutput = "Goodbye";
        response.tell(speechOutput);
    }
};

/* Gets a random new fact from the list and returns to the user. */

function handleNewFactRequest(response) {
    
//    var itemVal     = numvar; 
    
//    var itemSlot    = intent.slots.Number;
//    if (itemSlot && itemSlot.value){
//        itemVal = itemSlot.value;
//    }
    
//    var sayNum = itemVal.toString();
   
    
//    ref.on("value", function(snapshot) {
//
//        var allq        = snapshot.val();
//        quoteArr        = remakeArray(allq);
//        
//        var factIndex   = Math.floor(Math.random() * quoteArr.length);
//        var fact        = quoteArr[factIndex].textBlock;
//        var author      = quoteArr[factIndex].author;
//        var links       = quoteArr[factIndex].address;
//        
//var speechOutput        = "Here's an Inspirational Quote: " + fact + "  . " + author;
//var speechCardOutput    = "Here's your Inspirational Quote: " + fact + "<br> by: " + author + "<br> Link:" + links;
//var cardTitle           = "Inspirational Quote ";
//
//         response.tellWithCard(speechOutput,  cardTitle, speechCardOutput);
//        
//    }, function (errorObject) {
//        console.log("The read failed: " + errorObject.code);
//    });
//
//    
     var allq = snapshot.val();
        quoteArr = remakeArray(allq);
        var factIndex = Math.floor(Math.random() * quoteArr.length);
        console.log(quoteArr[factIndex].author);

        var fact    = quoteArr[factIndex].textBlock;
        var author  = quoteArr[factIndex].author;
        var speechOutput = response.Number + "Here's an Inspirational Quote: " + fact + "  By:  "+author;
        var speechBody = "More " + fact + "  By:  "+author;
        var cardTitle = "Inspirational Quote ";
        var speechCardOutput = "Here's your Inspirational Quote: " + fact + "<br> by: "+author;
    
         response.tellWithCard(speechOutput,  cardTitle, speechCardOutput);
    
    
    
    
    
}


// Helper
function remakeArray(obj) {
    var tempArr = [];
    var tempItem = [];
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop)){
            tempItem = obj[prop]
            tempArr.push(tempItem)
        }
    }
    //console.log(tempArr);
     return tempArr;
}
    


// Create the handler that responds to the Alexa Request.
exports.handler = function (event, context) {
    // Create an instance of the SpaceGeek skill.
    var spaceGeek = new SpaceGeek();
    spaceGeek.execute(event, context);
};

